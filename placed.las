%problem entities
color(red).
color(green).
color(blue).
color(yellow).
color(white1).
color(white2).
color(white3).
color(white4).
robot(psm1).
robot(psm2).
time(1..33).





prev(T1,T2) :- time(T1), time(T2), T2 = T1+1.

flag(V0,V1,V2,V3) :- at(V0,peg,V1,V2), release(V0,V2), prev(V2,V3).
initiated(placed(ring,V1,peg,V3,V4)) :- in_hand(V0,ring,V1,V2), flag(V0,V3,V2,V4).

%first try: PROBLEM, not working properly
%terminated(placed(ring,V1,peg,V3,V4)) :- in_hand(V0,ring,V1,V2), placed(ring,V1,peg,V3,V2), prev(V2,V4).

%second try removing wrong rule 
terminated(placed(ring,V0,peg,V1,V4)) :- placed(ring,V0,peg,V1,V2), extract(V3,ring,V0,V2), prev(V2,V4).

%HUMAN EXECUTION


#pos(a, {
    terminated(placed(ring,red,peg,white4,4)),
    terminated(placed(ring,green,peg,white1,20)),
    terminated(placed(ring,blue,peg,white3,12)),
    terminated(placed(ring,yellow,peg,white2,28)),

    initiated(placed(ring,red,peg,red,9)),
    initiated(placed(ring,yellow,peg,yellow,33)),
    initiated(placed(ring,blue,peg,blue,17)),
    initiated(placed(ring,green,peg,green,25))
}, {
    terminated(placed(ring,red,peg,white4,1)),
    terminated(placed(ring,red,peg,white4,2)),
    terminated(placed(ring,red,peg,white4,3)),
    terminated(placed(ring,red,peg,white4,5)),
    terminated(placed(ring,red,peg,white4,6)),
    terminated(placed(ring,red,peg,white4,7)),
    terminated(placed(ring,red,peg,white4,8)),
    terminated(placed(ring,red,peg,white4,9)),
    terminated(placed(ring,red,peg,white4,10)),
    terminated(placed(ring,red,peg,white4,11)),
    terminated(placed(ring,red,peg,white4,12)),
    terminated(placed(ring,red,peg,white4,13)),
    terminated(placed(ring,red,peg,white4,14)),
    terminated(placed(ring,red,peg,white4,15)),
    terminated(placed(ring,red,peg,white4,16)),
    terminated(placed(ring,red,peg,white4,17)),
    terminated(placed(ring,red,peg,white4,18)),
    terminated(placed(ring,red,peg,white4,19)),
    terminated(placed(ring,red,peg,white4,20)),
    terminated(placed(ring,red,peg,white4,21)),
    terminated(placed(ring,red,peg,white4,22)),
    terminated(placed(ring,red,peg,white4,23)),
    terminated(placed(ring,red,peg,white4,24)),
    terminated(placed(ring,red,peg,white4,25)),
    terminated(placed(ring,red,peg,white4,26)),
    terminated(placed(ring,red,peg,white4,27)),
    terminated(placed(ring,red,peg,white4,28)),
    terminated(placed(ring,red,peg,white4,29)),
    terminated(placed(ring,red,peg,white4,30)),
    terminated(placed(ring,red,peg,white4,31)),
    terminated(placed(ring,red,peg,white4,32)),
    terminated(placed(ring,red,peg,white4,33)),
    terminated(placed(ring,yellow,peg,white2,1)),
    terminated(placed(ring,yellow,peg,white2,2)),
    terminated(placed(ring,yellow,peg,white2,3)),
    terminated(placed(ring,yellow,peg,white2,4)),
    terminated(placed(ring,yellow,peg,white2,5)),
    terminated(placed(ring,yellow,peg,white2,6)),
    terminated(placed(ring,yellow,peg,white2,7)),
    terminated(placed(ring,yellow,peg,white2,8)),
    terminated(placed(ring,yellow,peg,white2,9)),
    terminated(placed(ring,yellow,peg,white2,10)),
    terminated(placed(ring,yellow,peg,white2,11)),
    terminated(placed(ring,yellow,peg,white2,12)),
    terminated(placed(ring,yellow,peg,white2,13)),
    terminated(placed(ring,yellow,peg,white2,14)),
    terminated(placed(ring,yellow,peg,white2,15)),
    terminated(placed(ring,yellow,peg,white2,16)),
    terminated(placed(ring,yellow,peg,white2,17)),
    terminated(placed(ring,yellow,peg,white2,18)),
    terminated(placed(ring,yellow,peg,white2,19)),
    terminated(placed(ring,yellow,peg,white2,20)),
    terminated(placed(ring,yellow,peg,white2,21)),
    terminated(placed(ring,yellow,peg,white2,22)),
    terminated(placed(ring,yellow,peg,white2,23)),
    terminated(placed(ring,yellow,peg,white2,24)),
    terminated(placed(ring,yellow,peg,white2,25)),
    terminated(placed(ring,yellow,peg,white2,26)),
    terminated(placed(ring,yellow,peg,white2,27)),
    terminated(placed(ring,yellow,peg,white2,29)),
    terminated(placed(ring,yellow,peg,white2,30)),
    terminated(placed(ring,yellow,peg,white2,31)),
    terminated(placed(ring,yellow,peg,white2,32)),
    terminated(placed(ring,yellow,peg,white2,33)),
    terminated(placed(ring,green,peg,white1,1)),
    terminated(placed(ring,green,peg,white1,2)),
    terminated(placed(ring,green,peg,white1,3)),
    terminated(placed(ring,green,peg,white1,4)),
    terminated(placed(ring,green,peg,white1,5)),
    terminated(placed(ring,green,peg,white1,6)),
    terminated(placed(ring,green,peg,white1,7)),
    terminated(placed(ring,green,peg,white1,8)),
    terminated(placed(ring,green,peg,white1,9)),
    terminated(placed(ring,green,peg,white1,10)),
    terminated(placed(ring,green,peg,white1,11)),
    terminated(placed(ring,green,peg,white1,12)),
    terminated(placed(ring,green,peg,white1,13)),
    terminated(placed(ring,green,peg,white1,14)),
    terminated(placed(ring,green,peg,white1,15)),
    terminated(placed(ring,green,peg,white1,16)),
    terminated(placed(ring,green,peg,white1,17)),
    terminated(placed(ring,green,peg,white1,18)),
    terminated(placed(ring,green,peg,white1,19)),
    terminated(placed(ring,green,peg,white1,21)),
    terminated(placed(ring,green,peg,white1,22)),
    terminated(placed(ring,green,peg,white1,23)),
    terminated(placed(ring,green,peg,white1,24)),
    terminated(placed(ring,green,peg,white1,25)),
    terminated(placed(ring,green,peg,white1,26)),
    terminated(placed(ring,green,peg,white1,27)),
    terminated(placed(ring,green,peg,white1,28)),
    terminated(placed(ring,green,peg,white1,29)),
    terminated(placed(ring,green,peg,white1,30)),
    terminated(placed(ring,green,peg,white1,31)),
    terminated(placed(ring,green,peg,white1,32)),
    terminated(placed(ring,green,peg,white1,33)),
    terminated(placed(ring,blue,peg,white3,1)),
    terminated(placed(ring,blue,peg,white3,2)),
    terminated(placed(ring,blue,peg,white3,3)),
    terminated(placed(ring,blue,peg,white3,4)),
    terminated(placed(ring,blue,peg,white3,5)),
    terminated(placed(ring,blue,peg,white3,6)),
    terminated(placed(ring,blue,peg,white3,7)),
    terminated(placed(ring,blue,peg,white3,8)),
    terminated(placed(ring,blue,peg,white3,9)),
    terminated(placed(ring,blue,peg,white3,10)),
    terminated(placed(ring,blue,peg,white3,11)),
    terminated(placed(ring,blue,peg,white3,13)),
    terminated(placed(ring,blue,peg,white3,14)),
    terminated(placed(ring,blue,peg,white3,15)),
    terminated(placed(ring,blue,peg,white3,16)),
    terminated(placed(ring,blue,peg,white3,17)),
    terminated(placed(ring,blue,peg,white3,18)),
    terminated(placed(ring,blue,peg,white3,19)),
    terminated(placed(ring,blue,peg,white3,20)),
    terminated(placed(ring,blue,peg,white3,21)),
    terminated(placed(ring,blue,peg,white3,22)),
    terminated(placed(ring,blue,peg,white3,23)),
    terminated(placed(ring,blue,peg,white3,24)),
    terminated(placed(ring,blue,peg,white3,25)),
    terminated(placed(ring,blue,peg,white3,26)),
    terminated(placed(ring,blue,peg,white3,27)),
    terminated(placed(ring,blue,peg,white3,28)),
    terminated(placed(ring,blue,peg,white3,29)),
    terminated(placed(ring,blue,peg,white3,30)),
    terminated(placed(ring,blue,peg,white3,31)),
    terminated(placed(ring,blue,peg,white3,32)),
    terminated(placed(ring,blue,peg,white3,33)),
    terminated(placed(ring,red,peg,red,1)),
    terminated(placed(ring,red,peg,red,2)),
    terminated(placed(ring,red,peg,red,3)),
    terminated(placed(ring,red,peg,red,4)),
    terminated(placed(ring,red,peg,red,5)),
    terminated(placed(ring,red,peg,red,6)),
    terminated(placed(ring,red,peg,red,7)),
    terminated(placed(ring,red,peg,red,8)),
    terminated(placed(ring,red,peg,red,9)),
    terminated(placed(ring,red,peg,red,10)),
    terminated(placed(ring,red,peg,red,11)),
    terminated(placed(ring,red,peg,red,12)),
    terminated(placed(ring,red,peg,red,13)),
    terminated(placed(ring,red,peg,red,14)),
    terminated(placed(ring,red,peg,red,15)),
    terminated(placed(ring,red,peg,red,16)),
    terminated(placed(ring,red,peg,red,17)),
    terminated(placed(ring,red,peg,red,18)),
    terminated(placed(ring,red,peg,red,19)),
    terminated(placed(ring,red,peg,red,20)),
    terminated(placed(ring,red,peg,red,21)),
    terminated(placed(ring,red,peg,red,22)),
    terminated(placed(ring,red,peg,red,23)),
    terminated(placed(ring,red,peg,red,24)),
    terminated(placed(ring,red,peg,red,25)),
    terminated(placed(ring,red,peg,red,26)),
    terminated(placed(ring,red,peg,red,27)),
    terminated(placed(ring,red,peg,red,28)),
    terminated(placed(ring,red,peg,red,29)),
    terminated(placed(ring,red,peg,red,30)),
    terminated(placed(ring,red,peg,red,31)),
    terminated(placed(ring,red,peg,red,32)),
    terminated(placed(ring,red,peg,red,33)),
    terminated(placed(ring,yellow,peg,yellow,1)),
    terminated(placed(ring,yellow,peg,yellow,2)),
    terminated(placed(ring,yellow,peg,yellow,3)),
    terminated(placed(ring,yellow,peg,yellow,4)),
    terminated(placed(ring,yellow,peg,yellow,5)),
    terminated(placed(ring,yellow,peg,yellow,6)),
    terminated(placed(ring,yellow,peg,yellow,7)),
    terminated(placed(ring,yellow,peg,yellow,8)),
    terminated(placed(ring,yellow,peg,yellow,9)),
    terminated(placed(ring,yellow,peg,yellow,10)),
    terminated(placed(ring,yellow,peg,yellow,11)),
    terminated(placed(ring,yellow,peg,yellow,12)),
    terminated(placed(ring,yellow,peg,yellow,13)),
    terminated(placed(ring,yellow,peg,yellow,14)),
    terminated(placed(ring,yellow,peg,yellow,15)),
    terminated(placed(ring,yellow,peg,yellow,16)),
    terminated(placed(ring,yellow,peg,yellow,17)),
    terminated(placed(ring,yellow,peg,yellow,18)),
    terminated(placed(ring,yellow,peg,yellow,19)),
    terminated(placed(ring,yellow,peg,yellow,20)),
    terminated(placed(ring,yellow,peg,yellow,21)),
    terminated(placed(ring,yellow,peg,yellow,22)),
    terminated(placed(ring,yellow,peg,yellow,23)),
    terminated(placed(ring,yellow,peg,yellow,24)),
    terminated(placed(ring,yellow,peg,yellow,25)),
    terminated(placed(ring,yellow,peg,yellow,26)),
    terminated(placed(ring,yellow,peg,yellow,27)),
    terminated(placed(ring,yellow,peg,yellow,28)),
    terminated(placed(ring,yellow,peg,yellow,29)),
    terminated(placed(ring,yellow,peg,yellow,30)),
    terminated(placed(ring,yellow,peg,yellow,31)),
    terminated(placed(ring,yellow,peg,yellow,32)),
    terminated(placed(ring,yellow,peg,yellow,33)),
    terminated(placed(ring,blue,peg,blue,1)),
    terminated(placed(ring,blue,peg,blue,2)),
    terminated(placed(ring,blue,peg,blue,3)),
    terminated(placed(ring,blue,peg,blue,4)),
    terminated(placed(ring,blue,peg,blue,5)),
    terminated(placed(ring,blue,peg,blue,6)),
    terminated(placed(ring,blue,peg,blue,7)),
    terminated(placed(ring,blue,peg,blue,8)),
    terminated(placed(ring,blue,peg,blue,9)),
    terminated(placed(ring,blue,peg,blue,10)),
    terminated(placed(ring,blue,peg,blue,11)),
    terminated(placed(ring,blue,peg,blue,12)),
    terminated(placed(ring,blue,peg,blue,13)),
    terminated(placed(ring,blue,peg,blue,14)),
    terminated(placed(ring,blue,peg,blue,15)),
    terminated(placed(ring,blue,peg,blue,16)),
    terminated(placed(ring,blue,peg,blue,17)),
    terminated(placed(ring,blue,peg,blue,18)),
    terminated(placed(ring,blue,peg,blue,19)),
    terminated(placed(ring,blue,peg,blue,20)),
    terminated(placed(ring,blue,peg,blue,21)),
    terminated(placed(ring,blue,peg,blue,22)),
    terminated(placed(ring,blue,peg,blue,23)),
    terminated(placed(ring,blue,peg,blue,24)),
    terminated(placed(ring,blue,peg,blue,25)),
    terminated(placed(ring,blue,peg,blue,26)),
    terminated(placed(ring,blue,peg,blue,27)),
    terminated(placed(ring,blue,peg,blue,28)),
    terminated(placed(ring,blue,peg,blue,29)),
    terminated(placed(ring,blue,peg,blue,30)),
    terminated(placed(ring,blue,peg,blue,31)),
    terminated(placed(ring,blue,peg,blue,32)),
    terminated(placed(ring,blue,peg,blue,33)),
    terminated(placed(ring,green,peg,green,33)),

    initiated(placed(ring,red,peg,red,1)),
    initiated(placed(ring,red,peg,red,2)),
    initiated(placed(ring,red,peg,red,3)),
    initiated(placed(ring,red,peg,red,4)),
    initiated(placed(ring,red,peg,red,5)),
    initiated(placed(ring,red,peg,red,6)),
    initiated(placed(ring,red,peg,red,7)),
    initiated(placed(ring,red,peg,red,8)),
    initiated(placed(ring,red,peg,red,10)),
    initiated(placed(ring,red,peg,red,11)),
    initiated(placed(ring,red,peg,red,12)),
    initiated(placed(ring,red,peg,red,13)),
    initiated(placed(ring,red,peg,red,14)),
    initiated(placed(ring,red,peg,red,15)),
    initiated(placed(ring,red,peg,red,16)),
    initiated(placed(ring,red,peg,red,17)),
    initiated(placed(ring,red,peg,red,18)),
    initiated(placed(ring,red,peg,red,19)),
    initiated(placed(ring,red,peg,red,20)),
    initiated(placed(ring,red,peg,red,21)),
    initiated(placed(ring,red,peg,red,22)),
    initiated(placed(ring,red,peg,red,23)),
    initiated(placed(ring,red,peg,red,24)),
    initiated(placed(ring,red,peg,red,25)),
    initiated(placed(ring,red,peg,red,26)),
    initiated(placed(ring,red,peg,red,27)),
    initiated(placed(ring,red,peg,red,28)),
    initiated(placed(ring,red,peg,red,29)),
    initiated(placed(ring,red,peg,red,30)),
    initiated(placed(ring,red,peg,red,31)),
    initiated(placed(ring,red,peg,red,32)),
    initiated(placed(ring,red,peg,red,33)),
    initiated(placed(ring,blue,peg,blue,1)),
    initiated(placed(ring,blue,peg,blue,2)),
    initiated(placed(ring,blue,peg,blue,3)),
    initiated(placed(ring,blue,peg,blue,4)),
    initiated(placed(ring,blue,peg,blue,5)),
    initiated(placed(ring,blue,peg,blue,6)),
    initiated(placed(ring,blue,peg,blue,7)),
    initiated(placed(ring,blue,peg,blue,8)),
    initiated(placed(ring,blue,peg,blue,10)),
    initiated(placed(ring,blue,peg,blue,11)),
    initiated(placed(ring,blue,peg,blue,12)),
    initiated(placed(ring,blue,peg,blue,13)),
    initiated(placed(ring,blue,peg,blue,14)),
    initiated(placed(ring,blue,peg,blue,15)),
    initiated(placed(ring,blue,peg,blue,16)),
    initiated(placed(ring,blue,peg,blue,9)),
    initiated(placed(ring,blue,peg,blue,18)),
    initiated(placed(ring,blue,peg,blue,19)),
    initiated(placed(ring,blue,peg,blue,20)),
    initiated(placed(ring,blue,peg,blue,21)),
    initiated(placed(ring,blue,peg,blue,22)),
    initiated(placed(ring,blue,peg,blue,23)),
    initiated(placed(ring,blue,peg,blue,24)),
    initiated(placed(ring,blue,peg,blue,25)),
    initiated(placed(ring,blue,peg,blue,26)),
    initiated(placed(ring,blue,peg,blue,27)),
    initiated(placed(ring,blue,peg,blue,28)),
    initiated(placed(ring,blue,peg,blue,29)),
    initiated(placed(ring,blue,peg,blue,30)),
    initiated(placed(ring,blue,peg,blue,31)),
    initiated(placed(ring,blue,peg,blue,32)),
    initiated(placed(ring,blue,peg,blue,33)),
    initiated(placed(ring,green,peg,green,1)),
    initiated(placed(ring,green,peg,green,2)),
    initiated(placed(ring,green,peg,green,3)),
    initiated(placed(ring,green,peg,green,4)),
    initiated(placed(ring,green,peg,green,5)),
    initiated(placed(ring,green,peg,green,6)),
    initiated(placed(ring,green,peg,green,7)),
    initiated(placed(ring,green,peg,green,8)),
    initiated(placed(ring,green,peg,green,10)),
    initiated(placed(ring,green,peg,green,11)),
    initiated(placed(ring,green,peg,green,12)),
    initiated(placed(ring,green,peg,green,13)),
    initiated(placed(ring,green,peg,green,14)),
    initiated(placed(ring,green,peg,green,15)),
    initiated(placed(ring,green,peg,green,16)),
    initiated(placed(ring,green,peg,green,17)),
    initiated(placed(ring,green,peg,green,18)),
    initiated(placed(ring,green,peg,green,19)),
    initiated(placed(ring,green,peg,green,20)),
    initiated(placed(ring,green,peg,green,21)),
    initiated(placed(ring,green,peg,green,22)),
    initiated(placed(ring,green,peg,green,23)),
    initiated(placed(ring,green,peg,green,24)),
    initiated(placed(ring,green,peg,green,9)),
    initiated(placed(ring,green,peg,green,26)),
    initiated(placed(ring,green,peg,green,27)),
    initiated(placed(ring,green,peg,green,28)),
    initiated(placed(ring,green,peg,green,29)),
    initiated(placed(ring,green,peg,green,30)),
    initiated(placed(ring,green,peg,green,31)),
    initiated(placed(ring,green,peg,green,32)),
    initiated(placed(ring,green,peg,green,33)),
    initiated(placed(ring,yellow,peg,yellow,1)),
    initiated(placed(ring,yellow,peg,yellow,2)),
    initiated(placed(ring,yellow,peg,yellow,3)),
    initiated(placed(ring,yellow,peg,yellow,4)),
    initiated(placed(ring,yellow,peg,yellow,5)),
    initiated(placed(ring,yellow,peg,yellow,6)),
    initiated(placed(ring,yellow,peg,yellow,7)),
    initiated(placed(ring,yellow,peg,yellow,8)),
    initiated(placed(ring,yellow,peg,yellow,10)),
    initiated(placed(ring,yellow,peg,yellow,11)),
    initiated(placed(ring,yellow,peg,yellow,12)),
    initiated(placed(ring,yellow,peg,yellow,13)),
    initiated(placed(ring,yellow,peg,yellow,14)),
    initiated(placed(ring,yellow,peg,yellow,15)),
    initiated(placed(ring,yellow,peg,yellow,16)),
    initiated(placed(ring,yellow,peg,yellow,17)),
    initiated(placed(ring,yellow,peg,yellow,18)),
    initiated(placed(ring,yellow,peg,yellow,19)),
    initiated(placed(ring,yellow,peg,yellow,20)),
    initiated(placed(ring,yellow,peg,yellow,21)),
    initiated(placed(ring,yellow,peg,yellow,22)),
    initiated(placed(ring,yellow,peg,yellow,23)),
    initiated(placed(ring,yellow,peg,yellow,24)),
    initiated(placed(ring,yellow,peg,yellow,25)),
    initiated(placed(ring,yellow,peg,yellow,26)),
    initiated(placed(ring,yellow,peg,yellow,27)),
    initiated(placed(ring,yellow,peg,yellow,28)),
    initiated(placed(ring,yellow,peg,yellow,29)),
    initiated(placed(ring,yellow,peg,yellow,30)),
    initiated(placed(ring,yellow,peg,yellow,31)),
    initiated(placed(ring,yellow,peg,yellow,32)),
    initiated(placed(ring,yellow,peg,yellow,9)),
    initiated(placed(ring,red,peg,green,_)),
    initiated(placed(ring,red,peg,blue,_)),
    initiated(placed(ring,red,peg,yellow,_)),
    initiated(placed(ring,yellow,peg,green,_)),
    initiated(placed(ring,yellow,peg,blue,_)),
    initiated(placed(ring,yellow,peg,red,_)),
    initiated(placed(ring,green,peg,red,_)),
    initiated(placed(ring,green,peg,blue,_)),
    initiated(placed(ring,green,peg,yellow,_)),
    initiated(placed(ring,blue,peg,green,_)),
    initiated(placed(ring,blue,peg,red,_)),
    initiated(placed(ring,blue,peg,yellow,_)),
    initiated(placed(ring,_,peg,white1,_)),
    initiated(placed(ring,_,peg,white2,_)),
    initiated(placed(ring,_,peg,white3,_)),
    initiated(placed(ring,_,peg,white4,_))
}, {
    at(psm1,peg,red,8).
    at(psm1,peg,red,9).
    at(psm1,peg,red,10).
    at(psm1,peg,red,11).
    at(psm1,peg,red,12).
    at(psm1,peg,blue,16).
    at(psm1,peg,blue,17).
    at(psm2,peg,green,24).
    at(psm2,peg,green,25).
    at(psm2,peg,green,26).
    at(psm2,peg,green,27).
    at(psm2,peg,green,28).
    at(psm2,peg,yellow,32). 
    at(psm2,peg,yellow,33). 
    at(psm2,ring,red,2).
    at(psm2,ring,red,3).
    at(psm2,ring,blue,10).
    at(psm2,ring,blue,11).
    at(psm2,ring,green,21).
    at(psm2,ring,green,22).
    at(psm2,ring,green,23).
    at(psm2,ring,yellow,29).
    at(psm2,ring,yellow,30).
    at(psm2,ring,yellow,31).
    at(psm1,ring,red,5).
    at(psm1,ring,red,6).
    at(psm1,ring,red,7).
    at(psm1,ring,blue,13).
    at(psm1,ring,blue,14).
    at(psm1,ring,blue,15).
    at(psm1,ring,green,18).
    at(psm1,ring,green,19).
    at(psm1,ring,yellow,26).
    at(psm1,ring,yellow,27).
    placed(ring,blue,peg,white3,1).
    placed(ring,green,peg,white1,1).
    placed(ring,yellow,peg,white2,1).
    placed(ring,red,peg,white4,1).
    move(psm2,ring,red,1).
    grasp(psm2,ring,red,2).
    placed(ring,red,peg,white4,2).
    placed(ring,green,peg,white1,2).
    placed(ring,blue,peg,white3,2).
    placed(ring,yellow,peg,white2,2).
    in_hand(psm2,ring,red,3).
    extract(psm2,ring,red,3).
    placed(ring,yellow,peg,white2,3).
    placed(ring,blue,peg,white3,3).
    placed(ring,green,peg,white1,3).
    placed(ring,red,peg,white4,3).
    in_hand(psm2,ring,red,4).
    move(psm2,center,red,4).
    placed(ring,green,peg,white1,4).
    placed(ring,blue,peg,white3,4).
    placed(ring,yellow,peg,white2,4).
    in_hand(psm2,ring,red,5).
    grasp(psm1,ring,red,5).
    placed(ring,yellow,peg,white2,5).
    placed(ring,blue,peg,white3,5).
    placed(ring,green,peg,white1,5).
    at(psm2,center,5).
    in_hand(psm2,ring,red,6).
    in_hand(psm1,ring,red,6).
    placed(ring,green,peg,white1,6).
    placed(ring,blue,peg,white3,6).
    placed(ring,yellow,peg,white2,6).
    at(psm2,center,6).
    release(psm2,6).
    in_hand(psm1,ring,red,7).
    placed(ring,yellow,peg,white2,7).
    placed(ring,blue,peg,white3,7).
    placed(ring,green,peg,white1,7).
    move(psm1,peg,red,7).
    at(psm2,center,7).
    in_hand(psm1,ring,red,8).
    placed(ring,green,peg,white1,8).
    placed(ring,blue,peg,white3,8).
    placed(ring,yellow,peg,white2,8).
    at(psm2,center,8).
    release(psm1,8).
    move(psm2,ring,blue,9).
    placed(ring,yellow,peg,white2,9).
    placed(ring,blue,peg,white3,9).
    placed(ring,green,peg,white1,9).
    placed(ring,red,peg,red,9).
    at(psm2,center,9).
    grasp(psm2,ring,blue,10).
    placed(ring,red,peg,red,10).
    placed(ring,green,peg,white1,10).
    placed(ring,blue,peg,white3,10).
    placed(ring,yellow,peg,white2,10).
    in_hand(psm2,ring,blue,11).
    extract(psm2,ring,blue,11).
    placed(ring,yellow,peg,white2,11).
    placed(ring,blue,peg,white3,11).
    placed(ring,green,peg,white1,11).
    placed(ring,red,peg,red,11).
    in_hand(psm2,ring,blue,12).
    move(psm2,center,blue,12).
    placed(ring,red,peg,red,12).
    placed(ring,green,peg,white1,12).
    placed(ring,yellow,peg,white2,12).
    in_hand(psm2,ring,blue,13).
    grasp(psm1,ring,blue,13).
    placed(ring,yellow,peg,white2,13).
    placed(ring,green,peg,white1,13).
    placed(ring,red,peg,red,13).
    at(psm2,center,13).
    in_hand(psm2,ring,blue,14).
    in_hand(psm1,ring,blue,14).
    placed(ring,red,peg,red,14).
    placed(ring,green,peg,white1,14).
    placed(ring,yellow,peg,white2,14).
    at(psm2,center,14).
    release(psm2,14).
    in_hand(psm1,ring,blue,15).
    placed(ring,yellow,peg,white2,15).
    placed(ring,green,peg,white1,15).
    placed(ring,red,peg,red,15).
    move(psm1,peg,blue,15).
    at(psm2,center,15).
    in_hand(psm1,ring,blue,16).
    placed(ring,red,peg,red,16).
    placed(ring,green,peg,white1,16).
    placed(ring,yellow,peg,white2,16).
    at(psm2,center,16).
    release(psm1,16).
    move(psm1,ring,green,17).
    placed(ring,yellow,peg,white2,17).
    placed(ring,blue,peg,blue,17).
    placed(ring,green,peg,white1,17).
    placed(ring,red,peg,red,17).
    at(psm2,center,17).
    grasp(psm1,ring,green,18).
    placed(ring,red,peg,red,18).
    placed(ring,green,peg,white1,18).
    placed(ring,blue,peg,blue,18).
    placed(ring,yellow,peg,white2,18).
    at(psm2,center,18).
    in_hand(psm1,ring,green,19).
    extract(psm1,ring,green,19).
    placed(ring,yellow,peg,white2,19).
    placed(ring,blue,peg,blue,19).
    placed(ring,green,peg,white1,19).
    placed(ring,red,peg,red,19).
    at(psm2,center,19).
    in_hand(psm1,ring,green,20).
    move(psm1,center,green,20).
    placed(ring,red,peg,red,20).
    placed(ring,blue,peg,blue,20).
    placed(ring,yellow,peg,white2,20).
    at(psm2,center,20).
    in_hand(psm1,ring,green,21).
    grasp(psm2,ring,green,21).
    placed(ring,yellow,peg,white2,21).
    placed(ring,blue,peg,blue,21).
    placed(ring,red,peg,red,21).
    at(psm1,center,21).
    in_hand(psm2,ring,green,22).
    in_hand(psm1,ring,green,22).
    placed(ring,red,peg,red,22).
    placed(ring,blue,peg,blue,22).
    placed(ring,yellow,peg,white2,22).
    at(psm1,center,22).
    release(psm1,22).
    in_hand(psm2,ring,green,23).
    placed(ring,yellow,peg,white2,23).
    placed(ring,blue,peg,blue,23).
    placed(ring,red,peg,red,23).
    move(psm2,peg,green,23).
    at(psm1,center,23).
    in_hand(psm2,ring,green,24).
    placed(ring,red,peg,red,24).
    placed(ring,blue,peg,blue,24).
    placed(ring,yellow,peg,white2,24).
    at(psm1,center,24).
    release(psm2,24).
    move(psm1,ring,yellow,25).
    placed(ring,yellow,peg,white2,25).
    placed(ring,blue,peg,blue,25).
    placed(ring,green,peg,green,25).
    placed(ring,red,peg,red,25).
    at(psm1,center,25).
    grasp(psm1,ring,yellow,26).
    placed(ring,red,peg,red,26).
    placed(ring,green,peg,green,26).
    placed(ring,blue,peg,blue,26).
    placed(ring,yellow,peg,white2,26).
    in_hand(psm1,ring,yellow,27).
    extract(psm1,ring,yellow,27).
    placed(ring,yellow,peg,white2,27).
    placed(ring,blue,peg,blue,27).
    placed(ring,green,peg,green,27).
    placed(ring,red,peg,red,27).
    in_hand(psm1,ring,yellow,28).
    move(psm1,center,yellow,28).
    placed(ring,red,peg,red,28).
    placed(ring,green,peg,green,28).
    placed(ring,blue,peg,blue,28).
    in_hand(psm1,ring,yellow,29).
    grasp(psm2,ring,yellow,29).
    placed(ring,blue,peg,blue,29).
    placed(ring,green,peg,green,29).
    placed(ring,red,peg,red,29).
    at(psm1,center,29).
    in_hand(psm2,ring,yellow,30).
    in_hand(psm1,ring,yellow,30).
    placed(ring,red,peg,red,30).
    placed(ring,green,peg,green,30).
    placed(ring,blue,peg,blue,30).
    at(psm1,center,30).
    release(psm1,30).
    in_hand(psm2,ring,yellow,31).
    placed(ring,blue,peg,blue,31).
    placed(ring,green,peg,green,31).
    placed(ring,red,peg,red,31).
    move(psm2,peg,yellow,31).
    at(psm1,center,31).
    in_hand(psm2,ring,yellow,32).
    placed(ring,red,peg,red,32).
    placed(ring,green,peg,green,32).
    placed(ring,blue,peg,blue,32).
    at(psm1,center,32).
    release(psm2,32).
    placed(ring,yellow,peg,yellow,33).
    placed(ring,blue,peg,blue,33).
    placed(ring,green,peg,green,33).
    placed(ring,red,peg,red,33).
    at(psm1,center,33).
}).



#pos(b, {
    terminated(placed(ring,red,peg,blue,4)),
    terminated(placed(ring,blue,peg,red,9)),
    terminated(placed(ring,red,peg,white1,14)),

    initiated(placed(ring,red,peg,white1,6)),
    initiated(placed(ring,red,peg,red,16)),
    initiated(placed(ring,blue,peg,blue,11))
}, {
    terminated(placed(ring,red,peg,blue,1)),
    terminated(placed(ring,red,peg,blue,2)),
    terminated(placed(ring,red,peg,blue,3)),
    terminated(placed(ring,red,peg,blue,5)),
    terminated(placed(ring,red,peg,blue,6)),
    terminated(placed(ring,red,peg,blue,7)),
    terminated(placed(ring,red,peg,blue,8)),
    terminated(placed(ring,red,peg,blue,9)),
    terminated(placed(ring,red,peg,blue,10)),
    terminated(placed(ring,red,peg,blue,11)),
    terminated(placed(ring,red,peg,blue,12)),
    terminated(placed(ring,red,peg,blue,13)),
    terminated(placed(ring,red,peg,blue,14)),
    terminated(placed(ring,red,peg,blue,15)),
    terminated(placed(ring,red,peg,blue,16)),
    terminated(placed(ring,red,peg,white1,1)),
    terminated(placed(ring,red,peg,white1,2)),
    terminated(placed(ring,red,peg,white1,3)),
    terminated(placed(ring,red,peg,white1,5)),
    terminated(placed(ring,red,peg,white1,6)),
    terminated(placed(ring,red,peg,white1,7)),
    terminated(placed(ring,red,peg,white1,8)),
    terminated(placed(ring,red,peg,white1,9)),
    terminated(placed(ring,red,peg,white1,10)),
    terminated(placed(ring,red,peg,white1,11)),
    terminated(placed(ring,red,peg,white1,12)),
    terminated(placed(ring,red,peg,white1,13)),
    terminated(placed(ring,red,peg,white1,4)),
    terminated(placed(ring,red,peg,white1,15)),
    terminated(placed(ring,red,peg,white1,16)),
    terminated(placed(ring,blue,peg,red,1)),
    terminated(placed(ring,blue,peg,red,2)),
    terminated(placed(ring,blue,peg,red,3)),
    terminated(placed(ring,blue,peg,red,5)),
    terminated(placed(ring,blue,peg,red,6)),
    terminated(placed(ring,blue,peg,red,7)),
    terminated(placed(ring,blue,peg,red,8)),
    terminated(placed(ring,blue,peg,red,4)),
    terminated(placed(ring,blue,peg,red,10)),
    terminated(placed(ring,blue,peg,red,11)),
    terminated(placed(ring,blue,peg,red,12)),
    terminated(placed(ring,blue,peg,red,13)),
    terminated(placed(ring,blue,peg,red,14)),
    terminated(placed(ring,blue,peg,red,15)),
    terminated(placed(ring,blue,peg,red,16)),

    initiated(placed(ring,red,peg,white1,1)),
    initiated(placed(ring,red,peg,white1,2)),
    initiated(placed(ring,red,peg,white1,3)),
    initiated(placed(ring,red,peg,white1,4)),
    initiated(placed(ring,red,peg,white1,5)),
    initiated(placed(ring,red,peg,white1,7)),
    initiated(placed(ring,red,peg,white1,8)),
    initiated(placed(ring,red,peg,white1,9)),
    initiated(placed(ring,red,peg,white1,10)),
    initiated(placed(ring,red,peg,white1,11)),
    initiated(placed(ring,red,peg,white1,12)),
    initiated(placed(ring,red,peg,white1,13)),
    initiated(placed(ring,red,peg,white1,14)),
    initiated(placed(ring,red,peg,white1,15)),
    initiated(placed(ring,red,peg,white1,16)),
    initiated(placed(ring,red,peg,red,1)),
    initiated(placed(ring,red,peg,red,2)),
    initiated(placed(ring,red,peg,red,3)),
    initiated(placed(ring,red,peg,red,4)),
    initiated(placed(ring,red,peg,red,5)),
    initiated(placed(ring,red,peg,red,7)),
    initiated(placed(ring,red,peg,red,8)),
    initiated(placed(ring,red,peg,red,9)),
    initiated(placed(ring,red,peg,red,10)),
    initiated(placed(ring,red,peg,red,11)),
    initiated(placed(ring,red,peg,red,12)),
    initiated(placed(ring,red,peg,red,13)),
    initiated(placed(ring,red,peg,red,14)),
    initiated(placed(ring,red,peg,red,15)),
    initiated(placed(ring,red,peg,red,6)),
    initiated(placed(ring,blue,peg,blue,1)),
    initiated(placed(ring,blue,peg,blue,2)),
    initiated(placed(ring,blue,peg,blue,3)),
    initiated(placed(ring,blue,peg,blue,4)),
    initiated(placed(ring,blue,peg,blue,5)),
    initiated(placed(ring,blue,peg,blue,7)),
    initiated(placed(ring,blue,peg,blue,8)),
    initiated(placed(ring,blue,peg,blue,9)),
    initiated(placed(ring,blue,peg,blue,10)),
    initiated(placed(ring,blue,peg,blue,6)),
    initiated(placed(ring,blue,peg,blue,12)),
    initiated(placed(ring,blue,peg,blue,13)),
    initiated(placed(ring,blue,peg,blue,14)),
    initiated(placed(ring,blue,peg,blue,15)),
    initiated(placed(ring,blue,peg,blue,16))
}, {
    at(psm1,peg,white1,5).
    at(psm1,peg,white1,6).
    at(psm1,peg,blue,10).
    at(psm1,peg,blue,11).
    at(psm1,peg,red,15).
    at(psm1,peg,red,16).
    at(psm1,ring,red,2).
    at(psm1,ring,red,3).
    at(psm1,ring,blue,7).
    at(psm1,ring,blue,8).
    at(psm1,ring,red,12).
    at(psm1,ring,red,13).
    placed(ring,blue,peg,red,1).
    placed(ring,green,peg,green,1).
    placed(ring,red,peg,blue,1).
    move(psm1,ring,red,1).
    grasp(psm1,ring,red,2).
    placed(ring,red,peg,blue,2).
    placed(ring,green,peg,green,2).
    placed(ring,blue,peg,red,2).
    in_hand(psm1,ring,red,3).
    extract(psm1,ring,red,3).
    placed(ring,blue,peg,red,3).
    placed(ring,green,peg,green,3).
    placed(ring,red,peg,blue,3).
    in_hand(psm1,ring,red,4).
    placed(ring,green,peg,green,4).
    placed(ring,blue,peg,red,4).
    move(psm1,peg,white1,4).
    in_hand(psm1,ring,red,5).
    placed(ring,blue,peg,red,5).
    placed(ring,green,peg,green,5).
    release(psm1,5).
    move(psm1,ring,blue,6).
    placed(ring,red,peg,white1,6).
    placed(ring,green,peg,green,6).
    placed(ring,blue,peg,red,6).
    grasp(psm1,ring,blue,7).
    placed(ring,blue,peg,red,7).
    placed(ring,green,peg,green,7).
    placed(ring,red,peg,white1,7).
    in_hand(psm1,ring,blue,8).
    extract(psm1,ring,blue,8).
    placed(ring,red,peg,white1,8).
    placed(ring,green,peg,green,8).
    placed(ring,blue,peg,red,8).
    in_hand(psm1,ring,blue,9).
    placed(ring,green,peg,green,9).
    placed(ring,red,peg,white1,9).
    move(psm1,peg,blue,9).
    in_hand(psm1,ring,blue,10).
    placed(ring,red,peg,white1,10).
    placed(ring,green,peg,green,10).
    release(psm1,10).
    move(psm1,ring,red,11).
    placed(ring,blue,peg,blue,11).
    placed(ring,green,peg,green,11).
    placed(ring,red,peg,white1,11).
    grasp(psm1,ring,red,12).
    placed(ring,red,peg,white1,12).
    placed(ring,green,peg,green,12).
    placed(ring,blue,peg,blue,12).
    in_hand(psm1,ring,red,13).
    extract(psm1,ring,red,13).
    placed(ring,blue,peg,blue,13).
    placed(ring,green,peg,green,13).
    placed(ring,red,peg,white1,13).
    in_hand(psm1,ring,red,14).
    placed(ring,green,peg,green,14).
    placed(ring,blue,peg,blue,14).
    move(psm1,peg,red,14).
    in_hand(psm1,ring,red,15).
    placed(ring,blue,peg,blue,15).
    placed(ring,green,peg,green,15).
    release(psm1,15).
    placed(ring,red,peg,red,16).
    placed(ring,green,peg,green,16).
    placed(ring,blue,peg,blue,16).
}).





#modeh(initiated(placed(ring,var(color),peg,var(color),var(time)))).
%#modeh(terminated(placed(ring,var(color),peg,var(color),var(time)))).

#modeb(1,reachable(var(robot),ring,var(color)),(positive)).
#modeb(1,reachable(var(robot),peg,var(color)),(positive)).
#modeb(1,in_hand(var(robot),ring,var(color),var(time)),(positive)).
#modeb(1,at(var(robot),peg,var(color),var(time)),(positive)).
#modeb(1,at(var(robot),center,var(time)),(positive)).
#modeb(1,at(var(robot),ring,var(color),var(time)),(positive)).
%#modeb(1,placed(ring,var(color),peg,var(color),var(time)),(positive)).
#modeb(1,extract(var(robot),ring,var(color),var(time)),(positive)).
#modeb(1,release(var(robot),var(time)),(positive)).
#modeb(1,move(var(robot),peg,var(color),var(time)),(positive)).
#modeb(1,move(var(robot),ring,var(color),var(time)),(positive)).
#modeb(1,grasp(var(robot),ring,var(color),var(time)),(positive)).
#modeb(1,move(var(robot),center,var(color),var(time)),(positive)).
#modeb(1,prev(var(time),var(time)),(anti_reflexive,positive)).
%#modeb(1,flag(var(robot),var(color),var(time),var(time)),(positive)).

#maxv(4).
#maxhl(1).
#max_penalty(100).