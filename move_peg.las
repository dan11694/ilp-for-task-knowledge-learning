%problem entities
color(red).
color(green).
color(blue).
color(yellow).
color(white1).
color(white2).
color(white3).
color(white4).
robot(psm1).
robot(psm2).




%FIRST TEST




#pos(a, {
    move(psm1,peg,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
}).

#neg(c1, {
    move(psm1,peg,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
    on(ring,red,peg,white4).
}).

#neg(z1, {
    move(psm1,peg,green)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
}).

#neg(w1, {
    move(psm1,peg,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
}).



#pos(c, {  
    move(psm1,peg,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm1,ring,blue).
    at(psm1,ring,blue).
    at(psm2,center).
    closed_gripper(psm1).
}).

#neg(a1, {  
    move(psm1,peg,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm1,ring,blue).
    in_hand(psm2,ring,blue).
    at(psm1,ring,blue).
    at(psm2,center).
    closed_gripper(psm1).
    closed_gripper(psm2).
}).





%SECOND TEST






#pos(d, {  
    move(psm2,peg,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm2,ring,yellow).
    at(psm2,ring,yellow).
    at(psm1,center).
    closed_gripper(psm2).
}).

#neg(b1, {  
    move(psm2,peg,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm2,ring,yellow).
    in_hand(psm1,ring,yellow).
    at(psm2,ring,yellow).
    at(psm1,center).
    closed_gripper(psm2).
    closed_gripper(psm1).
}).









%THIRD TEST








#pos(e, {  
    move(psm1,peg,white4)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm1,ring,red).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    closed_gripper(psm1).
}).

#neg(neg_1, {  
    move(psm1,peg,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm1,ring,red).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    closed_gripper(psm1).
}).

#neg(d1, {  
    move(psm1,peg,white4)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm1,ring,red).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    on(ring,red,peg,blue).
    closed_gripper(psm1).
}).



#pos(f, {  
    move(psm1,peg,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    in_hand(psm1,ring,blue).
    on(ring,green,peg,green).
    closed_gripper(psm1).
}).

#neg(neg_2, {  
    move(psm1,peg,white4)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    in_hand(psm1,ring,blue).
    on(ring,green,peg,green).
    closed_gripper(psm1).
}).



#pos(g, {  
    move(psm1,peg,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm1,ring,red).
    on(ring,blue,peg,blue).
    on(ring,green,peg,green).
    closed_gripper(psm1).
}).










%FOURTH TEST







#pos(h, {  
    move(psm1,peg,blue),
    move(psm2,peg,yellow)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    in_hand(psm2,ring,yellow).
    in_hand(psm1,ring,blue).
    closed_gripper(psm1).
    closed_gripper(psm2).
}).



#pos(i, {  
    move(psm1,peg,red)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,yellow).
    on(ring,blue,peg,blue).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
}).







#modeha(move(var(robot),peg,var(color))).

#modeb(1,in_hand(var(robot),ring,var(color))).
#modeb(1,at(var(robot),ring,var(color))).
#modeb(1,at(var(robot),peg,var(color))).
#modeb(1,reachable(var(robot),ring,var(color))).
#modeb(1,reachable(var(robot),peg,var(color))).
#modeb(1,on(ring,var(color),peg,var(color))).
#modeb(1,at(var(robot),center)).
#modeb(1,move(var(robot),peg,var(color))).
#modeb(1,closed_gripper(var(robot))).

#maxv(3).
#maxhl(1).
#max_penalty(100).
