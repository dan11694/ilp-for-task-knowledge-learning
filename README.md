# ILP for task knowledge learning

This repository contains ILASP files, scripts and csv files for the paper "Inductive learning of answer set programs for autonomous surgical task planning" by Daniele Meli, Mohan Sridharan and Paolo Fiorini, accepted for publication on Springer's journal Machine Learning (link for citation coming soon). 

The folder quantitative_test contains the script to automatically launch simulated scenarios, and the csv files with data for plots in the paper.