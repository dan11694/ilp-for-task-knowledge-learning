%problem entities
color(red).
color(green).
color(blue).
color(yellow).
color(white1).
color(white2).
color(white3).
color(white4).
robot(psm1).
robot(psm2).
time(1..33).





prev(T1,T2) :- time(T1), time(T2), T2 = T1+1.





%HUMAN EXECUTION


#pos(a, {
    closed_gripper(psm2,3),
    closed_gripper(psm2,4),
    closed_gripper(psm2,5),
    closed_gripper(psm2,6),
    closed_gripper(psm1,6),
    closed_gripper(psm1,7),
    closed_gripper(psm1,8),
    closed_gripper(psm1,11),
    closed_gripper(psm1,12),
    closed_gripper(psm1,13),
    closed_gripper(psm2,14),
    closed_gripper(psm1,14),
    closed_gripper(psm2,15),
    closed_gripper(psm2,16),
    closed_gripper(psm2,19),
    closed_gripper(psm2,20),
    closed_gripper(psm2,21),
    closed_gripper(psm2,22),
    closed_gripper(psm1,22),
    closed_gripper(psm1,23),
    closed_gripper(psm1,24),
    closed_gripper(psm1,27),
    closed_gripper(psm1,28),
    closed_gripper(psm1,29),
    closed_gripper(psm2,30),
    closed_gripper(psm1,30),
    closed_gripper(psm2,31),
    closed_gripper(psm2,32)
}, {
    closed_gripper(psm1,1),
    closed_gripper(psm1,2),
    closed_gripper(psm1,3),
    closed_gripper(psm1,4),
    closed_gripper(psm1,5),
    closed_gripper(psm1,9),
    closed_gripper(psm1,10),
    closed_gripper(psm1,15),
    closed_gripper(psm1,16),
    closed_gripper(psm1,17),
    closed_gripper(psm1,18),
    closed_gripper(psm1,19),
    closed_gripper(psm1,20),
    closed_gripper(psm1,21),
    closed_gripper(psm1,25),
    closed_gripper(psm1,26),
    closed_gripper(psm1,1),
    closed_gripper(psm1,31),
    closed_gripper(psm1,32),
    closed_gripper(psm1,33),
    closed_gripper(psm2,1),
    closed_gripper(psm2,2),
    closed_gripper(psm2,7),
    closed_gripper(psm2,8),
    closed_gripper(psm2,9),
    closed_gripper(psm2,10),
    closed_gripper(psm2,11),
    closed_gripper(psm2,12),
    closed_gripper(psm2,13),
    closed_gripper(psm2,17),
    closed_gripper(psm2,18),
    closed_gripper(psm2,23),
    closed_gripper(psm2,24),
    closed_gripper(psm2,25),
    closed_gripper(psm2,26),
    closed_gripper(psm2,27),
    closed_gripper(psm2,28),
    closed_gripper(psm2,29),
    closed_gripper(psm2,33)
}, {
    at(psm2,center,5).
    at(psm2,center,6).
    at(psm2,center,7).
    at(psm2,center,8).
    at(psm2,center,9).
    at(psm2,center,10).
    at(psm2,center,11).
    at(psm2,center,12).
    at(psm2,center,21).
    at(psm2,center,22).
    at(psm2,center,23).
    at(psm2,center,24).
    at(psm2,center,25).
    at(psm2,center,26).
    at(psm2,center,27).
    at(psm2,center,28).
    at(psm1,center,13).
    at(psm1,center,14).
    at(psm1,center,15).
    at(psm1,center,16).
    at(psm1,center,17).
    at(psm1,center,18).
    at(psm1,center,19).
    at(psm1,center,20).
    at(psm1,center,29).
    at(psm1,center,30).
    at(psm1,center,31).
    at(psm1,center,32).
    at(psm1,center,33).
    at(psm1,peg,red,8).
    at(psm1,peg,red,9).
    at(psm1,peg,blue,24).
    at(psm1,peg,blue,25).
    at(psm2,peg,yellow,16).
    at(psm2,peg,yellow,17).
    at(psm2,peg,green,32).
    at(psm2,peg,green,33).
    on(ring,red,peg,white1,1).
    on(ring,blue,peg,white2,1).
    on(ring,green,peg,white3,1).
    on(ring,yellow,peg,white4,1).
    move(psm2,ring,red,1).
    at(psm2,ring,red,2).
    grasp(psm2,ring,red,2).
    on(ring,red,peg,white1,2).
    on(ring,green,peg,white3,2).
    on(ring,blue,peg,white2,2).
    on(ring,yellow,peg,white4,2).
    at(psm2,ring,red,3).
    in_hand(psm2,ring,red,3).
    extract(psm2,ring,red,3).
    on(ring,yellow,peg,white4,3).
    on(ring,blue,peg,white2,3).
    on(ring,green,peg,white3,3).
    on(ring,red,peg,white1,3).
    in_hand(psm2,ring,red,4).
    move(psm2,center,red,4).
    on(ring,green,peg,white3,4).
    on(ring,blue,peg,white2,4).
    on(ring,yellow,peg,white4,4).
    at(psm1,ring,red,5).
    in_hand(psm2,ring,red,5).
    grasp(psm1,ring,red,5).
    on(ring,yellow,peg,white4,5).
    on(ring,blue,peg,white2,5).
    on(ring,green,peg,white3,5).
    at(psm1,ring,red,6).
    in_hand(psm2,ring,red,6).
    in_hand(psm1,ring,red,6).
    on(ring,green,peg,white3,6).
    on(ring,blue,peg,white2,6).
    on(ring,yellow,peg,white4,6).
    release(psm2,6).
    at(psm1,ring,red,7).
    in_hand(psm1,ring,red,7).
    on(ring,yellow,peg,white4,7).
    on(ring,blue,peg,white2,7).
    on(ring,green,peg,white3,7).
    move(psm1,peg,red,7).
    in_hand(psm1,ring,red,8).
    on(ring,green,peg,white3,8).
    on(ring,blue,peg,white2,8).
    on(ring,yellow,peg,white4,8).
    release(psm1,8).
    move(psm1,ring,yellow,9).
    on(ring,yellow,peg,white4,9).
    on(ring,blue,peg,white2,9).
    on(ring,green,peg,white3,9).
    on(ring,red,peg,red,9).
    at(psm1,ring,yellow,10).
    grasp(psm1,ring,yellow,10).
    on(ring,red,peg,red,10).
    on(ring,green,peg,white3,10).
    on(ring,blue,peg,white2,10).
    on(ring,yellow,peg,white4,10).
    at(psm1,ring,yellow,11).
    in_hand(psm1,ring,yellow,11).
    extract(psm1,ring,yellow,11).
    on(ring,yellow,peg,white4,11).
    on(ring,blue,peg,white2,11).
    on(ring,green,peg,white3,11).
    on(ring,red,peg,red,11).
    in_hand(psm1,ring,yellow,12).
    move(psm1,center,yellow,12).
    on(ring,red,peg,red,12).
    on(ring,green,peg,white3,12).
    on(ring,blue,peg,white2,12).
    at(psm2,ring,yellow,13).
    in_hand(psm1,ring,yellow,13).
    grasp(psm2,ring,yellow,13).
    on(ring,blue,peg,white2,13).
    on(ring,green,peg,white3,13).
    on(ring,red,peg,red,13).
    at(psm2,ring,yellow,14).
    in_hand(psm2,ring,yellow,14).
    in_hand(psm1,ring,yellow,14).
    on(ring,red,peg,red,14).
    on(ring,green,peg,white3,14).
    on(ring,blue,peg,white2,14).
    release(psm1,14).
    at(psm2,ring,yellow,15).
    in_hand(psm2,ring,yellow,15).
    on(ring,blue,peg,white2,15).
    on(ring,green,peg,white3,15).
    on(ring,red,peg,red,15).
    move(psm2,peg,yellow,15).
    in_hand(psm2,ring,yellow,16).
    on(ring,red,peg,red,16).
    on(ring,green,peg,white3,16).
    on(ring,blue,peg,white2,16).
    release(psm2,16).
    move(psm2,ring,blue,17).
    on(ring,yellow,peg,yellow,17).
    on(ring,blue,peg,white2,17).
    on(ring,green,peg,white3,17).
    on(ring,red,peg,red,17).
    at(psm2,ring,blue,18).
    grasp(psm2,ring,blue,18).
    on(ring,red,peg,red,18).
    on(ring,green,peg,white3,18).
    on(ring,blue,peg,white2,18).
    on(ring,yellow,peg,yellow,18).
    at(psm2,ring,blue,19).
    in_hand(psm2,ring,blue,19).
    extract(psm2,ring,blue,19).
    on(ring,yellow,peg,yellow,19).
    on(ring,blue,peg,white2,19).
    on(ring,green,peg,white3,19).
    on(ring,red,peg,red,19).
    in_hand(psm2,ring,blue,20).
    move(psm2,center,blue,20).
    on(ring,red,peg,red,20).
    on(ring,green,peg,white3,20).
    on(ring,yellow,peg,yellow,20).
    at(psm1,ring,blue,21).
    in_hand(psm2,ring,blue,21).
    grasp(psm1,ring,blue,21).
    on(ring,yellow,peg,yellow,21).
    on(ring,green,peg,white3,21).
    on(ring,red,peg,red,21).
    at(psm1,ring,blue,22).
    in_hand(psm2,ring,blue,22).
    in_hand(psm1,ring,blue,22).
    on(ring,red,peg,red,22).
    on(ring,green,peg,white3,22).
    on(ring,yellow,peg,yellow,22).
    release(psm2,22).
    at(psm1,ring,blue,23).
    in_hand(psm1,ring,blue,23).
    on(ring,yellow,peg,yellow,23).
    on(ring,green,peg,white3,23).
    on(ring,red,peg,red,23).
    move(psm1,peg,blue,23).
    in_hand(psm1,ring,blue,24).
    on(ring,red,peg,red,24).
    on(ring,green,peg,white3,24).
    on(ring,yellow,peg,yellow,24).
    release(psm1,24).
    move(psm1,ring,green,25).
    on(ring,yellow,peg,yellow,25).
    on(ring,blue,peg,blue,25).
    on(ring,green,peg,white3,25).
    on(ring,red,peg,red,25).
    at(psm1,ring,green,26).
    grasp(psm1,ring,green,26).
    on(ring,red,peg,red,26).
    on(ring,green,peg,white3,26).
    on(ring,blue,peg,blue,26).
    on(ring,yellow,peg,yellow,26).
    at(psm1,ring,green,27).
    in_hand(psm1,ring,green,27).
    extract(psm1,ring,green,27).
    on(ring,yellow,peg,yellow,27).
    on(ring,blue,peg,blue,27).
    on(ring,green,peg,white3,27).
    on(ring,red,peg,red,27).
    in_hand(psm1,ring,green,28).
    move(psm1,center,green,28).
    on(ring,red,peg,red,28).
    on(ring,blue,peg,blue,28).
    on(ring,yellow,peg,yellow,28).
    at(psm2,ring,green,29).
    in_hand(psm1,ring,green,29).
    grasp(psm2,ring,green,29).
    on(ring,yellow,peg,yellow,29).
    on(ring,blue,peg,blue,29).
    on(ring,red,peg,red,29).
    at(psm2,ring,green,30).
    in_hand(psm2,ring,green,30).
    in_hand(psm1,ring,green,30).
    on(ring,red,peg,red,30).
    on(ring,blue,peg,blue,30).
    on(ring,yellow,peg,yellow,30).
    release(psm1,30).
    at(psm2,ring,green,31).
    in_hand(psm2,ring,green,31).
    on(ring,yellow,peg,yellow,31).
    on(ring,blue,peg,blue,31).
    on(ring,red,peg,red,31).
    move(psm2,peg,green,31).
    in_hand(psm2,ring,green,32).
    on(ring,red,peg,red,32).
    on(ring,blue,peg,blue,32).
    on(ring,yellow,peg,yellow,32).
    release(psm2,32).
    on(ring,yellow,peg,yellow,33).
    on(ring,blue,peg,blue,33).
    on(ring,green,peg,green,33).
    on(ring,red,peg,red,33).
}).



% PEGS OCCUPIED


%#pos(b, {
%    at(psm1,peg,white4,5),
%    at(psm1,peg,white4,6),
%    at(psm1,peg,red,10),
%    at(psm1,peg,red,11),
%    at(psm1,peg,blue,15),
%    at(psm1,peg,blue,16)
%}, {
%    at(psm2,peg,_,_),
%    at(psm1,peg,green,_),
%    at(psm1,peg,yellow,_),
%    at(psm1,peg,white1,_),
%    at(psm1,peg,white2,_),
%    at(psm1,peg,white3,_),
%    at(psm1,peg,white4,1),
%    at(psm1,peg,white4,2),
%    at(psm1,peg,white4,3),
%    at(psm1,peg,white4,4),
%    at(psm1,peg,white4,7),
%    at(psm1,peg,white4,8),
%    at(psm1,peg,white4,9),
%    at(psm1,peg,white4,10),
%    at(psm1,peg,white4,11),
%    at(psm1,peg,white4,12),
%    at(psm1,peg,white4,13),
%    at(psm1,peg,white4,14),
%    at(psm1,peg,white4,15),
%    at(psm1,peg,white4,16),
%    at(psm1,peg,red,1),
%    at(psm1,peg,red,2),
%    at(psm1,peg,red,3),
%    at(psm1,peg,red,4),
%    at(psm1,peg,red,5),
%    at(psm1,peg,red,6),
%    at(psm1,peg,red,7),
%    at(psm1,peg,red,8),
%    at(psm1,peg,red,9),
%    at(psm1,peg,red,12),
%    at(psm1,peg,red,13),
%    at(psm1,peg,red,14),
%    at(psm1,peg,red,15),
%    at(psm1,peg,red,16),
%    at(psm1,peg,blue,1),
%    at(psm1,peg,blue,2),
%    at(psm1,peg,blue,3),
%    at(psm1,peg,blue,4),
%    at(psm1,peg,blue,5),
%    at(psm1,peg,blue,6),
%    at(psm1,peg,blue,7),
%    at(psm1,peg,blue,8),
%    at(psm1,peg,blue,9),
%    at(psm1,peg,blue,10),
%    at(psm1,peg,blue,11),
%    at(psm1,peg,blue,12),
%    at(psm1,peg,blue,13),
%    at(psm1,peg,blue,14)
%}, {
%    on(ring,red,peg,blue,1).
%    on(ring,blue,peg,red,1).
%    on(ring,green,peg,green,1).
%    move(psm1,ring,blue,1).
%    at(psm1,ring,blue,2).
%    grasp(psm1,ring,blue,2).
%    on(ring,red,peg,blue,2).
%    on(ring,green,peg,green,2).
%    on(ring,blue,peg,red,2).
%    closed_gripper(psm1,3).
%    at(psm1,ring,blue,3).
%    in_hand(psm1,ring,blue,3).
%    extract(psm1,ring,blue,3).
%    on(ring,blue,peg,red,3).
%    on(ring,green,peg,green,3).
%    on(ring,red,peg,blue,3).
%    closed_gripper(psm1,4).
%    in_hand(psm1,ring,blue,4).
%    on(ring,red,peg,blue,4).
%    on(ring,green,peg,green,4).
%    move(psm1,peg,white4,4).
%    closed_gripper(psm1,5).
%    in_hand(psm1,ring,blue,5).
%    on(ring,green,peg,green,5).
%    on(ring,red,peg,blue,5).
%    release(psm1,5).
%    move(psm1,ring,red,6).
%    on(ring,red,peg,blue,6).
%    on(ring,green,peg,green,6).
%    on(ring,blue,peg,white4,6).
%    at(psm1,ring,red,7).
%    grasp(psm1,ring,red,7).
%    on(ring,blue,peg,white4,7).
%    on(ring,green,peg,green,7).
%    on(ring,red,peg,blue,7).
%    closed_gripper(psm1,8).
%    at(psm1,ring,red,8).
%    in_hand(psm1,ring,red,8).
%    extract(psm1,ring,red,8).
%    on(ring,red,peg,blue,8).
%    on(ring,green,peg,green,8).
%    on(ring,blue,peg,white4,8).
%    closed_gripper(psm1,9).
%    in_hand(psm1,ring,red,9).
%    on(ring,blue,peg,white4,9).
%    on(ring,green,peg,green,9).
%    move(psm1,peg,red,9).
%    closed_gripper(psm1,10).
%    in_hand(psm1,ring,red,10).
%    on(ring,green,peg,green,10).
%    on(ring,blue,peg,white4,10).
%    release(psm1,10).
%    move(psm1,ring,blue,11).
%    on(ring,blue,peg,white4,11).
%    on(ring,green,peg,green,11).
%    on(ring,red,peg,red,11).
%    at(psm1,ring,blue,12).
%    grasp(psm1,ring,blue,12).
%    on(ring,red,peg,red,12).
%    on(ring,green,peg,green,12).
%    on(ring,blue,peg,white4,12).
%    closed_gripper(psm1,13).
%    at(psm1,ring,blue,13).
%    in_hand(psm1,ring,blue,13).
%    extract(psm1,ring,blue,13).
%    on(ring,blue,peg,white4,13).
%    on(ring,green,peg,green,13).
%    on(ring,red,peg,red,13).
%    closed_gripper(psm1,14).
%    in_hand(psm1,ring,blue,14).
%    on(ring,red,peg,red,14).
%    on(ring,green,peg,green,14).
%    move(psm1,peg,blue,14).
%    closed_gripper(psm1,15).
%    in_hand(psm1,ring,blue,15).
%    on(ring,green,peg,green,15).
%    on(ring,red,peg,red,15).
%    release(psm1,15).
%    on(ring,red,peg,red,16).
%    on(ring,green,peg,green,16).
%    on(ring,blue,peg,blue,16).
%}).






#modeh(closed_gripper(var(robot),var(time))).
%#modeh(initiated(closed_gripper(var(robot),var(time))).
%#modeh(terminated(at(var(robot),peg,var(color)),var(time))).

#modeb(1,reachable(var(robot),ring,var(color)),(positive)).
#modeb(1,reachable(var(robot),peg,var(color)), (positive)).
#modeb(1,in_hand(var(robot),ring,var(color),var(time)), (positive)).
#modeb(1,at(var(robot),peg,var(color),var(time)), (positive)).
#modeb(1,at(var(robot),center,var(time)), (positive)).
#modeb(1,at(var(robot),ring,var(color),var(time)), (positive)).
#modeb(1,closed_gripper(var(robot),var(time)), (positive)).
#modeb(1,on(ring,var(color),peg,var(color),var(time)), (positive)).
#modeb(1,extract(var(robot),ring,var(color),var(time)), (positive)).
#modeb(1,release(var(robot),var(time)), (positive)).
#modeb(1,move(var(robot),peg,var(color),var(time)), (positive)).
#modeb(1,move(var(robot),ring,var(color),var(time)), (positive)).
#modeb(1,grasp(var(robot),ring,var(color),var(time)), (positive)).
#modeb(1,move(var(robot),center,var(color),var(time)), (positive)).
#modeb(1,prev(var(time),var(time)), (anti_reflexive,positive)).

#maxv(4).
#maxhl(1).
#max_penalty(20).
