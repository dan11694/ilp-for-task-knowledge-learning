%problem entities
color(red).
color(green).
color(blue).
color(yellow).
color(white1).
color(white2).
color(white3).
color(white4).
robot(psm1).
robot(psm2).



%FIRST TEST




#pos(a, {
    release(psm1) %,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    at(psm1,peg,red).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
}).


#pos(b, {  
    release(psm2) %,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm1,ring,blue).
    in_hand(psm2,ring,blue).
    at(psm1,ring,blue).
    at(psm2,center).
    closed_gripper(psm2).
    closed_gripper(psm1).
}).

#neg(a1, {  
    release(psm2) %,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm2,ring,blue).
    at(psm1,ring,blue).
    at(psm2,center).
    closed_gripper(psm2).
}).

#neg(b1, {  
    release(psm1) %,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm1,ring,blue).
    at(psm1,ring,blue).
    at(psm2,center).
    closed_gripper(psm1).
}).



#pos(d, {  
    release(psm1) %,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm1,peg,blue).
    in_hand(psm1,ring,blue).
    closed_gripper(psm1).
}).





%SECOND TEST






#pos(e, {  
    release(psm2) %,ring,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm2,peg,yellow).
    in_hand(psm2,ring,yellow).
    closed_gripper(psm2).
}).

#pos(f, {  
    release(psm1) %,ring,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm1,center).
    at(psm2,ring,yellow).
    in_hand(psm1,ring,yellow).
    in_hand(psm2,ring,yellow).
    closed_gripper(psm1).
    closed_gripper(psm2).
}).

#neg(c1, {  
    release(psm1) %,ring,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm1,center).
    at(psm2,ring,yellow).
    in_hand(psm1,ring,yellow).
    closed_gripper(psm1).
}).

#neg(d1, {  
    release(psm2) %,ring,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm1,center).
    at(psm2,ring,yellow).
    in_hand(psm2,ring,yellow).
    closed_gripper(psm2).
}).









%THIRD TEST








#pos(g, {  
    release(psm1) %,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,blue).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    at(psm1,peg,grey).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
}).



#pos(h, {  
    release(psm1) %,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    at(psm1,peg,blue).
    in_hand(psm1,ring,blue).
    closed_gripper(psm1).
}).



#pos(i, {  
    release(psm1) %,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,blue).
    on(ring,green,peg,green).
    at(psm1,peg,red).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
}).











%FOURTH TEST







#pos(j, {  
    release(psm1), %,ring,blue),
    release(psm2) %,ring,yellow)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,white1).
    on(ring,blue,peg,white4).
    at(psm1,peg,blue).
    at(psm2,peg,yellow).
    in_hand(psm1,ring,blue).
    in_hand(psm2,ring,yellow).
    closed_gripper(psm1).
    closed_gripper(psm2).
}).



#pos(k, {  
    release(psm1) %,ring,red)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,yellow).
    on(ring,blue,peg,blue).
    in_hand(psm1,ring,red).
    at(psm1,peg,red).
    closed_gripper(psm1).
}).






#modeha(release(var(robot))).

#modeb(1,in_hand(var(robot),ring,var(color))).
#modeb(1,at(var(robot),ring,var(color))).
#modeb(1,at(var(robot),peg,var(color))).
#modeb(1,reachable(var(robot),ring,var(color))).
#modeb(1,reachable(var(robot),peg,var(color))).
#modeb(1,on(ring,var(color),peg,var(color))).
#modeb(1,at(var(robot),center)).
#modeb(1,release(var(robot))).
#modeb(1,closed_gripper(var(robot))).

#maxv(2).
#maxhl(1).
#max_penalty(100).
