#!/usr/bin/env python

from clingo import Function as Fun, Control, SolveResult, Model
from std_msgs.msg import Bool
import copy
import time
import csv
from gringoParser import string2fun as parse
from itertools import combinations, product
from matplotlib import pyplot as plt

import numpy as np










class Solver(object):    
    def __init__(self):
        self.control = Control([])
        self.control.load('../test.lp') #LAUNCH IN THIS FOLDER
        self.control.ground([("base", [])])        
        self.atoms = []
        self.hidden_atoms = []
        self.action_time = 0  
        self.step = 1 #for iterative solver
        

    def on_model(self, model):
        self.atoms[:] = model.symbols(shown=True)
        self.hidden_atoms[:] = model.symbols(atoms=True)

    def solve(self, context):
        cont = ''
        for atom in context:
            self.control.assign_external(parse(atom), True)
            cont += atom + ', '
        # self.step = 1
        # init_step = self.step
        init_time = time.time()
        while time.time() - init_time < 200.:
            parts = []

            self.control.cleanup()
            parts.append(("check", [self.step]))
            self.control.release_external(Fun("query", [self.step-1]))

            parts.append(("step", [self.step]))

            self.control.ground(parts)
            self.control.assign_external(Fun("query", [self.step]), True)
            result = self.control.solve(on_model = self.on_model)
            self.step += 1
            if result.satisfiable:
                break
        
        if result.satisfiable:
            ordered_atoms = []
            if self.atoms != []:
                for atom in self.atoms:
                    if (atom.name == 'move' or atom.name == 'release' or atom.name == 'grasp' or atom.name == 'extract'):
                        tmp_list = []
                        tmp_list.append(atom.name) # ...name...
                        tmp_list.append(str(atom.arguments[0])) # [robot...
                        if tmp_list[0] != 'release':
                            tmp_list.append(str(atom.arguments[1])) # ...object...
                        else:
                            tmp_list.append('none') # ...object...
                        if tmp_list[2] != 'none':
                            tmp_list.append(str(atom.arguments[2])) # ...color... (if not move_center nor release)
                        else:
                            tmp_list.append('none')
                        tmp_list.append(int(str(atom.arguments[-1])) + self.action_time) # ...time]
                        ordered_atoms.append(tmp_list)
                ordered_atoms.sort(key = lambda action: action[-1])
            plan = ''
            for atom in ordered_atoms:
                plan += atom[0] + '(' + atom[1] + ',' + atom[2] + ',' + atom[3] + ',' + str(atom[4]) + '), '
            return time.time() - init_time, cont, plan
        else:
            return 1000., cont, ''












def main():

    # color_rings = ['red', 'green', 'blue', 'yellow']
    # #to consider rings on white pegs (standard task)
    # color_pegs = ['white1', 'white2', 'white3', 'white4']
    # dist_min = [1,2,3,4]
    # dist_max = [5,6,7,8]
    # for color in color_pegs:
    #     color_rings.append(color)

    # prods = list(product(color_rings[0:4], color_rings)) #specify [0:4] to first entry to consider only colored rings
    # for prod in prods:
    #     if prod[0] == prod[1]:
    #         prods.remove(prod)
    
    # combs = list(combinations(prods,4))
    # new_combs = []
    # for comb in combs:
    #     #remove combinations with same initial or final index (same ring on two pegs, or two rings on a peg, respectively)
    #     if len([item[0] for item in comb]) == len(set([item[0] for item in comb])) and len([item[-1] for item in comb]) == len(set([item[-1] for item in comb])):
    #         new_combs.append(comb)
            
    # f = open('every_pegs_ilp_partial_seq1_opt.csv', 'w')
    # writer = csv.writer(f, delimiter='|')
    # for comb in new_combs:
    #     context = ['reachable(psm1,peg,red)',
    #                 'reachable(psm1,peg,blue)',
    #                 'reachable(psm1,peg,white3)',
    #                 'reachable(psm1,peg,white4)',
    #                 'reachable(psm2,peg,white1)',
    #                 'reachable(psm2,peg,white2)',
    #                 'reachable(psm2,peg,green)',
    #                 'reachable(psm2,peg,yellow)']
    #     solver = Solver()
    #     i = 0
    #     for item in comb:
    #         context.append('placed(ring,'+item[0]+',peg,'+item[-1]+')')
    #         if item[-1] == 'red' or item[-1] == 'blue' or item[-1] == 'white3' or item[-1] == 'white4':
    #             context.append('reachable(psm1,ring,'+item[0]+')')
    #             #FOR OPTIMIZATION
    #             context.append('distance(psm1,ring,'+item[0]+','+str(dist_min[i])+')')
    #             context.append('distance(psm2,ring,'+item[0]+','+str(dist_max[i])+')')
    #             i = i+1
    #         else:
    #             context.append('reachable(psm2,ring,'+item[0]+')')
    #             #FOR OPTIMIZATION
    #             context.append('distance(psm2,ring,'+item[0]+','+str(dist_min[i])+')')
    #             context.append('distance(psm1,ring,'+item[0]+','+str(dist_max[i])+')')
    #             i = i+1

    #     time, cont, plan = solver.solve(context)
    #     writer.writerow([str(time), cont, plan])
  
    # f.close()








    # READ 
    files = ["every_pegs_iros_par.csv", "every_pegs_iros_par_opt.csv", "every_pegs_iros_par.csv"]
    data = []
    for filename in files:
        with open(filename) as file:
            reader = csv.reader(file, delimiter='|')
            data_single = []
            line_counter = 0
            for row in reader:
                if line_counter != 0:
                    data_single.append([float(row[0]), row[1], row[2]])
                line_counter += 1
            data.append(np.array(data_single))

    data = np.concatenate(np.array(data), 1)
    # parallel = list(data[:, :6])
    sequential = list(data[:, :9])

    # parallel.sort(key = lambda actions: actions[5].count("("))
    sequential.sort(key = lambda actions: actions[2].count("("))

    print([action[8].count("(") == 0 for action in sequential].count(True))

    prev_length = sequential[0][2].count("(")
    opt_time = [float(sequential[0][3])]
    orig_time = [float(sequential[0][0])]
    opt_times = []
    orig_times = []
    plan_size = [sequential[0][5].count("(")]
    plan_size_orig = []
    plan_size_opt = []
    i = 1
    while i < len(sequential):
        #PLANNING TIME
        curr_length = sequential[i][2].count("(")
        if curr_length == prev_length:
            opt_time.append(float(sequential[i][3]))
            orig_time.append(float(sequential[i][0]))
            plan_size.append(sequential[i][5].count("("))
        else:
            # if np.mean(opt_time) != 1000. and np.mean(orig_time) != 1000.:
            opt_times.append([np.mean(opt_time), np.std(opt_time)])
            orig_times.append([np.mean(orig_time), np.std(orig_time)])
            plan_size_opt.append([np.mean(plan_size), np.std(plan_size)])
            plan_size_orig.append(prev_length)
            prev_length = curr_length

            plan_size = [sequential[i][5].count("(")]
            opt_time = [float(sequential[i][3])]
            orig_time = [float(sequential[i][0])]
        
        i=i+1
    

    prev_length = sequential[0][2].count("(")
    simple_time = [float(sequential[0][6])]
    orig_time = [float(sequential[0][0])]
    simple_times = []
    orig_times = []
    plan_size = [sequential[0][8].count("(")]
    plan_size_orig = []
    plan_size_simple = []
    i = 1
    while i < len(sequential):
        #PLANNING TIME
        curr_length = sequential[i][2].count("(")
        if curr_length == prev_length:
            simple_time.append(float(sequential[i][6]))
            orig_time.append(float(sequential[i][0]))
            plan_size.append(sequential[i][8].count("("))
        else:
            # if np.mean(simple_time) != 1000. and np.mean(orig_time) != 1000.:
            simple_times.append([np.mean(simple_time), np.std(simple_time)])
            orig_times.append([np.mean(orig_time), np.std(orig_time)])
            plan_size_simple.append([np.mean(plan_size), np.std(plan_size)])
            plan_size_orig.append(prev_length)
            prev_length = curr_length

            plan_size = [sequential[i][8].count("(")]
            simple_time = [float(sequential[i][6])]
            orig_time = [float(sequential[i][0])]
        
        i=i+1

    # _, (ax0, ax1) = plt.subplots(nrows=2)
    plt.figure()
    # ax1.errorbar(np.linspace(0,len(plan_size_orig), len(plan_size_orig)), [time[0] for time in simple_times], yerr=[time[1] for time in simple_times], color='g', fmt='-o', label='Encoding 1', elinewidth=3, markeredgewidth=3, capsize=5)
    # plt.errorbar(np.linspace(0,len(plan_size_orig), len(plan_size_orig)), [min(time[0], 50) for time in orig_times], yerr=[time[1] for time in orig_times], color='r', fmt='-o', label='Optimization', elinewidth=3, markeredgewidth=3, capsize=5)
    plt.errorbar(plan_size_orig, [time[0] for time in orig_times], yerr=[time[1] for time in orig_times], color='b', fmt='-o', elinewidth=3, markeredgewidth=3, capsize=5)
    # ax0.errorbar(np.linspace(0,len(plan_size_orig), len(plan_size_orig)), [plan[0] for plan in plan_size_simple], [plan[1] for plan in plan_size_simple], color='g', fmt='-o', label='Encoding 1', elinewidth=3, markeredgewidth=3, capsize=5)
    # ax0.plot(np.linspace(0, len(plan_size_orig), len(plan_size_orig)), plan_size_orig, color='r', marker='o', label='Optmization')
    # ax0.errorbar(np.linspace(0,len(plan_size_orig), len(plan_size_orig)), [plan[0] for plan in plan_size_opt], [plan[1] for plan in plan_size_opt], color='b', fmt='-o', label='Simple', elinewidth=3, markeredgewidth=3, capsize=5)
    plt.ylabel("Planning time [s]", fontsize=35)
    plt.xlabel("Plan size (# actions)", fontsize=35)
    plt.yticks(fontsize=35)
    plt.xticks(fontsize=35)
    # ax1.set_ylabel('Planning time [s]', fontsize=23)
    # ax0.set_ylabel('Plan size', fontsize=23)
    # plt.xticks([])
    # ax0.set_xlim(left=0, right=len(plan_size_orig)+1)
    # ax1.set_xlim(left=0, right=len(plan_size_orig)+1)
    # ax0.set_xticks([])
    # ax1.set_xticks([])
    plt.xlim(xmax = plan_size_orig[-1]+1)
    # frame1 = plt.gca()
    # frame1.axes.xaxis.set_ticks([])
    # frame1.axes.yaxis.set_limits()
    # plt.legend(fontsize=35, loc='best')
    # ax0.tick_params(labelsize=23)
    # ax1.tick_params(labelsize=23)
    # ax0.grid()
    # ax1.grid()
    plt.grid()
    plt.show()







if __name__ == '__main__':
    main()
