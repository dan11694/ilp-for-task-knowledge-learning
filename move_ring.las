%problem entities
color(red).
color(green).
color(blue).
color(yellow).
color(white1).
color(white2).
color(white3).
color(white4).
robot(psm1).
robot(psm2).








%FIRST TEST




#pos(a, {
    move(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
}).

#neg(a1, {
    move(psm1,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
}).


#neg(b1, {
    move(psm2,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
}).

#pos(b, {  
    move(psm2,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).

#neg(c1, {  
    move(psm1,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).

#neg(d1, {  
    move(psm2,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).

#neg(t1, {  
    move(psm1,ring,_)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    closed_gripper(psm1).
}).



#pos(c, {  
    move(psm1,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).



#neg(e1, {  
    move(psm2,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).



#neg(f1, {  
    move(psm2,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).





%SECOND TEST






#pos(d, {  
    move(psm1,ring,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).

#neg(g1, {  
    move(psm2,ring,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).

#neg(h1, {  
    move(psm2,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
}).









%THIRD TEST








#pos(e, {  
    move(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,blue).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
}).

#neg(i1, {  
    move(psm2,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,blue).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
}).

#neg(j1, {  
    move(psm2,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,blue).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
}).



#pos(f, {  
    move(psm1,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
}).



#neg(k1, {  
    move(psm2,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
}).




#neg(l1, {  
    move(psm2,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
}).




#pos(g, {  
    move(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,blue).
    on(ring,green,peg,green).
}).


#neg(m1, {  
    move(psm2,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,blue).
    on(ring,green,peg,green).
}).


#neg(n1, {  
    move(psm2,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,blue).
    on(ring,green,peg,green).
}).










%FOURTH TEST







#pos(h, {  
    move(psm1,ring,blue),
    move(psm2,ring,yellow)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,white1).
    on(ring,blue,peg,white4).
}).

#neg(o1, {  
    move(psm1,ring,yellow)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,white1).
    on(ring,blue,peg,white4).
}).

#neg(p1, {  
    move(psm2,ring,blue)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,white1).
    on(ring,blue,peg,white4).
}).



#pos(i, {  
    move(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,yellow).
    on(ring,blue,peg,blue).
}).

#neg(q1, {  
    move(psm1,ring,yellow)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,yellow).
    on(ring,blue,peg,blue).
}).

#neg(r1, {  
    move(psm2,ring,blue)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,yellow).
    on(ring,blue,peg,blue).
}).

#neg(s1, {  
    move(psm2,ring,red)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,yellow).
    on(ring,blue,peg,blue).
}).







#modeha(move(var(robot),ring,var(color))).

#modeb(1,in_hand(var(robot),ring,var(color))).
#modeb(1,at(var(robot),ring,var(color))).
#modeb(1,at(var(robot),peg,var(color))).
#modeb(1,reachable(var(robot),ring,var(color))).
#modeb(1,reachable(var(robot),peg,var(color))).
#modeb(1,on(ring,var(color),peg,var(color))).
#modeb(1,at(var(robot),center)).
#modeb(1,move(var(robot),ring,var(color))).
#modeb(1,closed_gripper(var(robot))).

#maxv(2).
#maxhl(1).
#max_penalty(100).
