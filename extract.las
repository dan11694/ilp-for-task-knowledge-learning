%problem entities
color(red).
color(green).
color(blue).
color(yellow).
color(white1).
color(white2).
color(white3).
color(white4).
robot(psm1).
robot(psm2).



%FIRST TEST




#pos(a, {
    extract(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    in_hand(psm1,ring,red).
    at(psm1,ring,red).
}).









%THIRD TEST








#pos(e, {  
    extract(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,blue).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    in_hand(psm1,ring,red).
    at(psm1,ring,red).
}).



#pos(f, {  
    extract(psm1,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    in_hand(psm1,ring,blue).
    at(psm1,ring,blue).
}).



#pos(g, {  
    extract(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,blue).
    on(ring,green,peg,green).
    in_hand(psm1,ring,red).
    at(psm1,ring,red).
}).










%FOURTH TEST







#pos(h, {  
    extract(psm1,ring,blue),
    extract(psm2,ring,yellow)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,white1).
    on(ring,blue,peg,white4).
    in_hand(psm1,ring,blue).
    in_hand(psm2,ring,yellow).
    at(psm1,ring,blue).
    at(psm2,ring,yellow).
}).







#modeha(extract(var(robot),ring,var(color))).

#modeb(1,in_hand(var(robot),ring,var(color))).
%#modeb(1,at(var(robot),ring,var(color))).
%#modeb(1,at(var(robot),peg,var(color))).
#modeb(1,reachable(var(robot),ring,var(color))).
#modeb(1,reachable(var(robot),peg,var(color))).
#modeb(1,on(ring,var(color),peg,var(color))).
%#modeb(1,at(var(robot),center)).
#modeb(1,extract(var(robot),ring,var(color))).
#modeb(1,closed_gripper(var(robot))).

#maxv(2).
#maxhl(1).
#max_penalty(100).
