%problem entities
color(red).
color(green).
color(blue).
color(yellow).
color(white1).
color(white2).
color(white3).
color(white4).
robot(psm1).
robot(psm2).



%FIRST TEST



#pos(c, {  
    move(psm2,center,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm2,ring,blue).
    closed_gripper(psm2).
    at(psm2,ring,blue).
}).





%SECOND TEST






#pos(d, {  
    move(psm1,center,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    in_hand(psm1,ring,yellow).
    closed_gripper(psm1).
    at(psm1,ring,yellow).
}).










#neg(e, {  
    move(psm1,center,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white3).
    in_hand(psm1,ring,red).
    closed_gripper(psm1).
    at(psm1,ring,red).
}).











#modeha(move(var(robot),center,var(color))).

#modeb(1,in_hand(var(robot),ring,var(color))).
%#modeb(1,at(var(robot),ring,var(color))).
%#modeb(1,at(var(robot),peg,var(color))).
#modeb(1,reachable(var(robot),ring,var(color))).
#modeb(1,reachable(var(robot),peg,var(color))).
#modeb(1,on(ring,var(color),peg,var(color))).
%#modeb(1,at(var(robot),center)).
#modeb(1,move(var(robot),center,var(color))).
#modeb(1,closed_gripper(var(robot))).

#maxv(2).
#maxhl(1).
#max_penalty(100).
