%problem entities
color(red).
color(green).
color(blue).
color(yellow).
color(white1).
color(white2).
color(white3).
color(white4).
robot(psm1).
robot(psm2).



%FIRST TEST




#pos(a, {
    grasp(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    at(psm1,ring,red).
}).

#pos(b, {  
    grasp(psm2,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm2,ring,blue).
}).

#pos(c, {  
    grasp(psm1,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm2,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm1,ring,blue).
    in_hand(psm2,ring,blue).
    at(psm2,center).
}).



#pos(d, {  
    grasp(psm1,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm1,ring,blue).
}).





%SECOND TEST






#pos(e, {  
    grasp(psm1,ring,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm1,ring,yellow).
}).

#pos(f, {  
    grasp(psm2,ring,yellow)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,yellow).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,red).
    at(psm1,center).
    at(psm2,ring,yellow).
    in_hand(psm1,ring,yellow).
}).









%THIRD TEST








#pos(g, {  
    grasp(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,blue).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    at(psm1,ring,red).
}).



#pos(h, {  
    grasp(psm1,ring,blue)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,red).
    on(ring,green,peg,green).
    at(psm1,ring,blue).
}).



#pos(i, {  
    grasp(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,green).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,red,peg,white4).
    on(ring,blue,peg,blue).
    on(ring,green,peg,green).
    at(psm1,ring,red).
}).











%FOURTH TEST







#pos(j, {  
    grasp(psm1,ring,blue),
    grasp(psm2,ring,yellow)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm1,ring,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,white1).
    on(ring,blue,peg,white4).
    at(psm1,ring,blue).
    at(psm2,ring,yellow).
}).



#pos(k, {  
    grasp(psm1,ring,red)
}, {
}, {
    reachable(psm1,ring,blue).
    reachable(psm1,ring,red).
    reachable(psm1,peg,red).
    reachable(psm2,ring,yellow).
    reachable(psm1,peg,blue).
    reachable(psm2,peg,green).
    reachable(psm2,peg,yellow).
    reachable(psm1,peg,white3).
    reachable(psm1,peg,white4).
    reachable(psm2,peg,white1).
    reachable(psm2,peg,white2).
    on(ring,yellow,peg,yellow).
    on(ring,blue,peg,blue).
    at(psm1,ring,red).
}).







#modeha(grasp(var(robot),ring,var(color))).

#modeb(1,in_hand(var(robot),ring,var(color))).
#modeb(1,at(var(robot),ring,var(color))).
%#modeb(1,at(var(robot),peg,var(color))).
#modeb(1,reachable(var(robot),ring,var(color))).
#modeb(1,reachable(var(robot),peg,var(color))).
#modeb(1,on(ring,var(color),peg,var(color))).
#modeb(1,at(var(robot),center)).
#modeb(1,grasp(var(robot),ring,var(color))).
#modeb(1,closed_gripper(var(robot))).

#maxv(2).
#maxhl(1).
#max_penalty(100).
